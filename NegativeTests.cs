using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace MyTestProject
{
    [TestClass]
    public class UnitTest1
    {
        /* Negative Tests */
        [TestMethod]
        public void TestMethod6()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_06.exe C:\VS_Projects\Test_06.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod7()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_07.exe C:\VS_Projects\Test_07.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod8()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_08.exe C:\VS_Projects\Test_08.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod9()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_09.exe C:\VS_Projects\Test_09.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod10()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_10.exe C:\VS_Projects\Test_10.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }
    }
}
