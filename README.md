Nathan Connors

Assignment 5

ITAD 268 QA Methods

-- Positive Tests --

Test_01: null ?? null == null -> true

Test_02: null ?? (int?)1 == 1 -> true

Test_03: null ?? "" == "" -> true

Test_04: f() ?? null == null -> true //f() returns null

Test_05: f() ?? 1 == null -> true    //f() returns null


-- Negative Tests  --

Test_06: var x = (null ?? 1);

Test_07: var x = ("" ?? (int?)1);

Test_08: var x = ("" ?? 1);

Test_09: var x = (1 ?? 1);

Test_10: var x = (f() ?? 1);  //f() returns 1