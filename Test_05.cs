using System;
class Program
{
	public static int? f()
    {
        return null;
    }

    static void Main(string[] args)
    {
        try
        {
            Console.WriteLine((f() ?? 1) == 1);
        }
        catch (Exception)
        {
            Console.WriteLine(false);
        }

    }
}