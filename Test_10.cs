using System;
class Program
{
    public static int f()
    {
        return 1;
    }
    static void Main(string[] args)
    {
        try
        {
            var x = (f() ?? 1);
        }
        catch (Exception)
        {
            Console.WriteLine(false);
        }

    }
}