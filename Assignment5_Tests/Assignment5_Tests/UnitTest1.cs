﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace MyTestProject
{
    [TestClass]
    public class UnitTest1
    {
        /* Positive Tests */
        [TestMethod]
        public void TestMethod1()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_01.exe C:\VS_Projects\Test_01.cs";
            p.Start();

            p.WaitForExit();

            Process p1 = new Process();
            p1.StartInfo.FileName = @"C:\VS_Projects\Test_01.exe";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;
            p1.Start();

            p1.WaitForExit();
            string result = p1.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_02.exe C:\VS_Projects\Test_02.cs";
            p.Start();

            p.WaitForExit();

            Process p1 = new Process();
            p1.StartInfo.FileName = @"C:\VS_Projects\Test_02.exe";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;
            p1.Start();

            p1.WaitForExit();
            string result = p1.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod3()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_03.exe C:\VS_Projects\Test_03.cs";
            p.Start();

            p.WaitForExit();

            Process p1 = new Process();
            p1.StartInfo.FileName = @"C:\VS_Projects\Test_03.exe";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;
            p1.Start();

            p1.WaitForExit();
            string result = p1.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod4()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_04.exe C:\VS_Projects\Test_04.cs";
            p.Start();

            p.WaitForExit();

            Process p1 = new Process();
            p1.StartInfo.FileName = @"C:\VS_Projects\Test_04.exe";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;
            p1.Start();

            p1.WaitForExit();
            string result = p1.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        [TestMethod]
        public void TestMethod5()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_05.exe C:\VS_Projects\Test_05.cs";
            p.Start();

            p.WaitForExit();

            Process p1 = new Process();
            p1.StartInfo.FileName = @"C:\VS_Projects\Test_05.exe";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardOutput = true;
            p1.Start();

            p1.WaitForExit();
            string result = p1.StandardOutput.ReadToEnd();

            Assert.AreEqual(result, "True\r\n");
        }

        /* Negative Tests */
        [TestMethod]
        public void TestMethod6()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_06.exe C:\VS_Projects\Test_06.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod7()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_07.exe C:\VS_Projects\Test_07.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod8()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_08.exe C:\VS_Projects\Test_08.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod9()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_09.exe C:\VS_Projects\Test_09.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

        [TestMethod]
        public void TestMethod10()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\VS_Projects\Test_10.exe C:\VS_Projects\Test_10.cs";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();

            p.WaitForExit();

            string result = p.StandardOutput.ReadToEnd();
            Assert.IsTrue(result.Contains("CS0019"));

        }

    }
}